
var app = angular.module('myApp', []);

app.controller('MainCtrl', function($scope,$http) {
  
  $scope.alarmsList = [];

  // get all the alarms which are not resolved
  $http.get("https://demos.cumulocity.com/alarm/alarms?pageSize=2000&resolved=false", {
    headers: {'Authorization': "Basic ZWthdGVyaW5hOkthdGUwMTIzNA=="}
    })
    .success(function (alarmsList) { 
      $scope.alarmsList = alarmsList.alarms;
    })
    .then(function(){  
      //list of devices
      $scope.devices =[];
      // sorted list of alarms according to severity
      $scope.sortedAlarmArray =[];
      
      // object to make an order of severity
      var severitySortOrder = {CRITICAL:1,MAJOR:2,MINOR:3,WARNING:4};
      
      // sort an alarmList according to severity order
      $scope.alarmsList.sort(function (a, b) {
        return severitySortOrder[a.severity] - severitySortOrder[b.severity];
      });
      // copy alrmList array 
      $scope.recentActive = angular.copy($scope.alarmsList);
      
      // sort copied alarmList according to recent time
      $scope.recentActive.sort(function (a, b) {
        // if(a.status === 'ACTIVE' && b.status === 'ACTIVE'){
          return new Date(b.creationTime) - new Date(a.creationTime);  
        // }
      });
      
      // function to make array values unique
      var uniqueArray = function(arrArg) {
        return arrArg.filter(function(elem, pos,arr) {
          return arr.indexOf(elem) == pos;
        });
      };

      // get the list of devices
      $scope.alarmsList.map(function(alarm){
        $scope.devices.push(alarm.source.name);
      });
      $scope.devices = uniqueArray($scope.devices);

      // get all the alarms for a single device and make that device's list
      for(var i=0; i<$scope.devices.length;i++){
        $scope.tempAlarm =[];
        $scope.recentActive.map(function(alarm){
          if($scope.devices[i] === alarm.source.name){
            $scope.tempAlarm.push(alarm);
          }
        });
        $scope.sortedAlarmArray.push({name:$scope.devices[i], alarm: $scope.tempAlarm});
      }
    });
    
});

